//Integrate PostGreSql Database with strapi cms

First I will install strapi with npx create-strapi-app@latest my-project
Then we select quickstart in installation type
Then we will install postgresql and pgadmin 
Then we will enter configuration of postgresql in ./config/database.js 
    // strapi-api/config/database.js
    module.exports = ({ env }) => ({
      connection: {
        client: 'postgres',
        connection: {
          host: env('DATABASE_HOST', 'localhost'),
          port: env.int('DATABASE_PORT', 5432),
          database: env('DATABASE_NAME', 'bank'),
          user: env('DATABASE_USERNAME', 'postgres'),
          password: env('DATABASE_PASSWORD', '0000'),
          schema: env('DATABASE_SCHEMA', 'public'), // Not required
          ssl: {
            rejectUnauthorized: env.bool('DATABASE_SSL_SELF', false),
          },
        },
        debug: false,
      },
    });
Then we enter basic configuration of db like host, port, DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD etc. in ./config/database.js 
Then we go to url http://localhost:1337/admin/
Then in plugins section we create collection in content-type-builder, it is just like schema
And Insert Data in collection types
But these api will be authenticated, but if we want to make these apis will be public then we go to settings and then USERS & PERMISSIONS PLUGIN then select roles and then select collection types and make these apis will be public, and the routes of this apis will also shown, 
// How to Upload images to aws s3
First we will make aws-s3 bucket name, access key and secret key and then we make folder plugins.js inside ./config folder 

module.exports = ({ env }) => ({
  upload: {
    config: {
      provider: 'aws-s3',
      providerOptions: {
        accessKeyId: env('AWS_ACCESS_KEY_ID'),
        secretAccessKey: env('AWS_ACCESS_SECRET'),
        region: env('AWS_REGION'),
        params: {
          Bucket: env('AWS_BUCKET'),
        },
      },
      actionOptions: {
        upload: {},
        uploadStream: {},
        delete: {},
      },
    },
  },
});
      
      replace accesskeyId, secretAccessKey, region and BucketName of aws S3
      